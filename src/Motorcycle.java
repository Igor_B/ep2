public class Motorcycle extends Flex {
    public static final String NAME = "Motorcycle";
    public static final int INT_TYPE = 4;
    private static final String FUEL1_NAME = "gasolina";
    private static final String FUEL2_NAME = "alcool";
    private static final float EFFICIENCY1 = 50.0f;
    private static final float EFFICIENCY2 = 43.0f;
    private static final float EFFICIENCY_FACTOR1 = 0.3f;
    private static final float EFFICIENCY_FACTOR2 = 0.4f;
    private static final double MAXIMUM_WEIGHT = 50.0;
    private static final float SPEED = 110.0f;

    Motorcycle() {
        name = "Moto";
        setFuels(FUEL1_NAME, FUEL2_NAME);
        setEfficiencys(EFFICIENCY1, EFFICIENCY2);
        setEfficiencyFactors(EFFICIENCY_FACTOR1, EFFICIENCY_FACTOR2);
        setMaximumWeight(MAXIMUM_WEIGHT);
        setSpeed(SPEED);
    }
}
