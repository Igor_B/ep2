public abstract class Flex extends Vehicle {
    private Fuel[] fuels;
    private float[] efficiencys;
    private float[] efficiencyFactors;

    public Fuel[] getFuels() {
        return fuels;
    }

    public float[] getEfficiencys() {
        return efficiencys;
    }

    public float[] getEfficiencyFactors() {
        return efficiencyFactors;
    }

    public void setFuels(String name1, String name2) {
        fuels = new Fuel[2];
        fuels[0] = new Fuel(name1);
        fuels[1] = new Fuel(name2);
    }

    public void setEfficiencys(float efficiency1, float efficiency2) {
        efficiencys = new float[2];
        efficiencys[0] = efficiency1;
        efficiencys[1] = efficiency2;
    }

    public void setEfficiencyFactors(float efficiencyFactor1, float efficiencyFactor2) {
        efficiencyFactors = new float[2];
        efficiencyFactors[0] = efficiencyFactor1;
        efficiencyFactors[1] = efficiencyFactor2;
    }

    private double getMinCostForFuels(double cost1, double cost2) {
        if (cost1 < cost2) {
            return cost1;
        } else {
            return cost2;
        }
    }

    @Override
    public float getRealEfficiencyFactor(double weight, int fuelType) {
        double realEfficiency = ((double) efficiencys[fuelType]) - (((double) efficiencyFactors[fuelType]) * weight);

        return (float) realEfficiency;
    }

    @Override
    public double calculateCost(double weight, double distance) {
        double[] fuelsCost = new double[fuels.length];

        for (int i = 0; i < fuels.length; i++) {
            double realEfficiency = (double) getRealEfficiencyFactor(weight, i);
            double totalFuel = distance / realEfficiency;

            fuelsCost[i] = ((double) fuels[i].price) * totalFuel;
        }

        return getMinCostForFuels(fuelsCost[0], fuelsCost[1]);
    }
}
