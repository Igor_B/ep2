public class Truck extends NonFlex {
    public static final String NAME = "Truck";
    public static final int INT_TYPE = 1;
    private static final String FUEL_NAME = "diesel";
    private static final float EFFICIENCY = 8.0f;
    private static final float EFFICIENCY_FACTOR = 0.0002f;
    private static final double MAXIMUM_WEIGHT = 30000;
    private static final float SPEED = 60.0f;

    Truck() {
        name = "Caminhao";
        setFuel(FUEL_NAME);
        setEfficiency(EFFICIENCY);
        setEfficiencyFactor(EFFICIENCY_FACTOR);
        setMaximumWeight(MAXIMUM_WEIGHT);
        setSpeed(SPEED);
    }
}
