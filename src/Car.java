public class Car extends Flex {
    public static final String NAME = "Car";
    public static final int INT_TYPE = 3;
    private static final String FUEL1_NAME = "gasolina";
    private static final String FUEL2_NAME = "alcool";
    private static final float EFFICIENCY1 = 14.0f;
    private static final float EFFICIENCY2 = 12.0f;
    private static final float EFFICIENCY_FACTOR1 = 0.025f;
    private static final float EFFICIENCY_FACTOR2 = 0.0231f;
    private static final double MAXIMUM_WEIGHT = 360.0;
    private static final float SPEED = 100.0f;

    Car() {
        name = "Carro";
        setFuels(FUEL1_NAME, FUEL2_NAME);
        setEfficiencys(EFFICIENCY1, EFFICIENCY2);
        setEfficiencyFactors(EFFICIENCY_FACTOR1, EFFICIENCY_FACTOR2);
        setMaximumWeight(MAXIMUM_WEIGHT);
        setSpeed(SPEED);
    }
}
