import java.util.List;
import java.util.ArrayList;

public class Fleet {
    public List<Van> vans;
    public List<Car> cars;
    public List<Truck> trucks;
    public List<Motorcycle> motorcycles;

    // { TRUCKS, VANS, CARS, MOTORCYCLES }
    private int[] availableVehicles = { 0, 0, 0, 0 };

    Fleet() {
        vans = new ArrayList<>();
        cars = new ArrayList<>();
        trucks = new ArrayList<>();
        motorcycles = new ArrayList<>();
    }

    public void setAvailableVehicles() {
        availableVehicles[0] = trucks.size();
        availableVehicles[1] = vans.size();
        availableVehicles[2] = cars.size();
        availableVehicles[3] = motorcycles.size();
    }

    public List<FreightStatus> receiveFreight(double weight, double distance, double time) {
        if (verifyFreight(weight, distance, time)) { return null; }

        List<String> possibleVehicles = foundVehicles(weight, distance, time);

        return classifyPossibleVehicles(possibleVehicles, weight, distance);
    }

    private double[] getArrayExtremeValue(double[] array, boolean maximum) {
        double smallest;
        double smallestPosition;
        double smallestData[] = new double[2];

        smallest = array[0];
        smallestPosition = 0;

        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array. length; j++) {
                if (array[j] < smallest && array[j] > 0 && !maximum) {
                    smallest = array[j];
                    smallestPosition = (double) j;
                }
                if (array[j] > smallest && array[j] > 0 && maximum) {
                    smallest = array[j];
                    smallestPosition = (double) j;
                }
            }
        }

        smallestData[0] = smallest;
        smallestData[1] = smallestPosition;

        return smallestData;
    }

    private double[] getBetterCostBenefit(double[] costs, double[] times) {
        double[] costBenifits = new double[4];

        for (int i = 0; i < costs.length; i++) {
            costBenifits[i] = costs[i] / times[i];
        }

        return getArrayExtremeValue(costBenifits, true);
    }

    private List<FreightStatus> getFreightOptions(int[] vehicleTypes, double[] costs, double[] times) {
        List<FreightStatus> freightOptions = new ArrayList<>();

        for (int i = 0; i < 3; i++) {
            switch (vehicleTypes[i]) {
                case Truck.INT_TYPE:
                    freightOptions.add(new FreightStatus(new Truck(), costs[i], times[i]));

                    break;
                case Van.INT_TYPE:
                    freightOptions.add(new FreightStatus(new Van(), costs[i], times[i]));

                    break;
                case Car.INT_TYPE:
                    freightOptions.add(new FreightStatus(new Car(), costs[i], times[i]));

                    break;
                case Motorcycle.INT_TYPE:
                    freightOptions.add(new FreightStatus(new Motorcycle(), costs[i], times[i]));

                    break;
                default:
                    break;
            }
        }

        return freightOptions;
    }

    private List<FreightStatus> setVehiclesFreightCategory(double[] costs, double[] times, int[] vehicleTypes) {
        double[] costsData = getArrayExtremeValue(costs, false);

        double[] timesData = getArrayExtremeValue(times, false);

        double[] bestCostBenefit = getBetterCostBenefit(costs, times);

        int intPositionCost = (int) costsData[1];

        int vehicleTypeForCost = vehicleTypes[intPositionCost];

        int intPositionTime = (int) timesData[1];

        int vehicleTypeForTime = vehicleTypes[intPositionTime];

        int intPositionCostBenefit = (int) bestCostBenefit[1];

        int vehicleTypeForCostBenefit = vehicleTypes[intPositionCostBenefit];

        double[] toPassCosts = new double[3];
        double[] toPassTimes = new double[3];
        int[] toPassVehicleTypes = new int[3];

        toPassVehicleTypes[0] = vehicleTypeForCost;
        toPassVehicleTypes[1] = vehicleTypeForTime;
        toPassVehicleTypes[2] = vehicleTypeForCostBenefit;


        int position = 0;
        for (int i = 0; i < 3; i++) {
            if (i == 0) { position = intPositionCost; }
            if (i == 1) { position = intPositionTime; }
            if (i == 2) { position = intPositionCostBenefit; }

            toPassCosts[i] = costs[position];
            toPassTimes[i] = times[position];
        }

        return getFreightOptions(toPassVehicleTypes, toPassCosts, toPassTimes);
    }

    private List<FreightStatus> classifyPossibleVehicles(List<String> possibleVehicles,
       double weight, double distance) {
        Car car = new Car();
        Van van = new Van();
        Truck truck = new Truck();
        Motorcycle motorcycle = new Motorcycle();

        int[] vehicleTypes = new int[4];
        double[] travelCosts = new double[4];
        double[] travelTimes = new double[4];


        int i = 0;
        for (String name : possibleVehicles) {

            switch (name) {
                case Truck.NAME:
                    travelTimes[i] = truck.calculateTime(distance);
                    travelCosts[i] = truck.calculateCost(weight, distance);

                    vehicleTypes[i] = 1;

                    i = i + 1;
                    break;
                case Van.NAME:
                    travelTimes[i] = van.calculateTime(distance);
                    travelCosts[i] = van.calculateCost(weight, distance);

                    vehicleTypes[i] = 2;

                    i = i + 1;

                    break;
                case Car.NAME:
                    travelTimes[i] = car.calculateTime(distance);
                    travelCosts[i] = car.calculateCost(weight, distance);

                    vehicleTypes[i] = 3;

                    i = i + 1;

                    break;
                case Motorcycle.NAME:
                    travelTimes[i] = motorcycle.calculateTime(distance);
                    travelCosts[i] = motorcycle.calculateCost(weight, distance);

                    vehicleTypes[i] = 4;

                    i = i + 1;

                    break;
            }
        }

        return setVehiclesFreightCategory(travelCosts, travelTimes, vehicleTypes);
    }

    private boolean hasAvailableVehicles() {
        for (int number : availableVehicles) {
            if (number > 0) { return true; }
        }

        return false;
    }

    public boolean verifyFreight(double weight, double distance, double time) {
        Motorcycle motorcycle = new Motorcycle();

        boolean canCarryIt = weight > 30000.0;

        double transportFastestTime = motorcycle.calculateTime(distance);

        boolean carryInTime = transportFastestTime <= time;

        return canCarryIt && carryInTime && hasAvailableVehicles();
    }

    public List<String> foundVehicles(double weight, double distance, double time) {
        Car car = new Car();
        Van van = new Van();
        Truck truck = new Truck();
        Motorcycle motorcycle = new Motorcycle();

        List<String> possibleVehicles = new ArrayList<>();

        boolean carryCondition;


        for (int i = 1; i < 5; i++) {
            switch (i) {
                case 1:
                    carryCondition = (truck.calculateTime(distance) <= time &&
                        availableVehicles[0] > 0 && truck.canCarryIt(weight));

                    if (carryCondition) {
                        possibleVehicles.add(Truck.NAME);
                    }

                    break;
                case 2:
                    carryCondition = (van.calculateTime(distance) <= time &&
                       availableVehicles[1] > 0 && van.canCarryIt(weight));

                    if (carryCondition) {
                        possibleVehicles.add(Van.NAME);
                    }

                    break;
                case 3:
                    carryCondition = (car.calculateTime(distance) <= time &&
                       availableVehicles[2] > 0 && car.canCarryIt(weight));

                    if (carryCondition) {
                        possibleVehicles.add(Car.NAME);
                    }

                    break;
                case 4:
                    carryCondition = (motorcycle.calculateTime(distance) <= time &&
                       availableVehicles[3] > 0 && motorcycle.canCarryIt(weight));

                    if (carryCondition) {
                        possibleVehicles.add(Motorcycle.NAME);
                    }

                    break;
            }
        }

        return possibleVehicles;
    }
}
