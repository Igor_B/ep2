import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

public class Main {
	public static void main(String[] args) {
        Company company = new Company();

        List<FreightStatus> list = company.toTestFreight(
        	3500.0,
        	4000.0,
        	200.0
        );

        int i = 1;
        for (FreightStatus f : list) {
            String vehicleType = f.vehicle.toString();
            
            System.out.println("\n" + i + "º");
            
            if (vehicleType.contains(Truck.NAME)) {
            	System.out.println("Veículo: " + Truck.NAME);
            }
            if (vehicleType.contains(Van.NAME)) {
            	System.out.println("Veículo: " + Van.NAME);
            }
            if (vehicleType.contains(Car.NAME)) {
            	System.out.println("Veículo: " + Car.NAME);
            }
            if (vehicleType.contains(Motorcycle.NAME)) {
            	System.out.println("Veículo: " + Motorcycle.NAME);
            }
            
            BigDecimal decimalCost = new BigDecimal(f.totalCost).setScale(2, RoundingMode.HALF_EVEN);
            System.out.println(decimalCost.doubleValue());
            
            BigDecimal decimalTime = new BigDecimal(f.freightTime).setScale(2, RoundingMode.HALF_EVEN);
            System.out.println(decimalTime.doubleValue());

            i++;
        }
	}
}
