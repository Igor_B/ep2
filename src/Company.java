import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Company {
    private final String COMPANY_STATUS_FILE= "doc/status.txt";

    public Fleet fleet;
    private float profitMargin;

    Company() {
        fleet = new Fleet();

        configureCompany();
    }

    private void configureCompany() {
        if (checkFleet()) {
            List<String> fileData = readCompanyStatusFile();

            setFleet(fileData);
            this.fleet.setAvailableVehicles();
            setProfitMargin(fileData);
        } else {
            generateCompanyFile();
        }
    }

    private boolean checkFleet() {
        return new File(COMPANY_STATUS_FILE).isFile();
    }

    private void setProfitMargin(List<String> fileData) {
        this.profitMargin = Float.parseFloat(fileData.get(0));
    }

    private void generateCompanyFile() {
        try {
            OutputStream os = new FileOutputStream(COMPANY_STATUS_FILE);
            OutputStreamWriter osw = new OutputStreamWriter(os);
            BufferedWriter bw = new BufferedWriter(osw);

            for (int i = 0; i < 5; i++) {
                bw.write("0\n");
            }

            bw.close();
        }
        catch (IOException e) {
            System.out.println("Ocorreu algum erro... :(");
        }
    }

    private void createVehicles(String fileLine, int position) {
        int number = Integer.parseInt(fileLine);

        switch (position) {
            case Truck.INT_TYPE:
                this.fleet.trucks.addAll(Arrays.asList(new Truck[number]));

                break;
            case Van.INT_TYPE:
                this.fleet.vans.addAll(Arrays.asList(new Van[number]));

                break;
            case Car.INT_TYPE:
                this.fleet.cars.addAll(Arrays.asList(new Car[number]));

                break;
            case Motorcycle.INT_TYPE:
                this.fleet.motorcycles.addAll(Arrays.asList(new Motorcycle[number]));

                break;
            default:
                break;
        }
    }

    private List<String> readCompanyStatusFile() {
        List<String> fileLines = new ArrayList<>();

        try {
            InputStream is = new FileInputStream(COMPANY_STATUS_FILE);
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);

            String fileLine = br.readLine();

            while (fileLine != null) {
                fileLines.add(fileLine);

                fileLine = br.readLine();
            }

            br.close();

            return fileLines;
        }
        catch (IOException e) {
            System.out.println("Ocorreu algum erro... :(");

            return fileLines;
        }
    }

    private void clearFleet() {
        this.fleet.trucks.clear();
        this.fleet.vans.clear();
        this.fleet.cars.clear();
        this.fleet.motorcycles.clear();
    }

    private void setFleet(List<String> fileData) {
        for (int i = 1; i < fileData.size(); i++) {
            createVehicles(fileData.get(i), i);
        }
    }

    private int selectOperation(int number, int variation, char operation) {
        switch (operation) {
            case '+':
                number = number + variation;
                break;
            case '-':
                if (variation > number) {
                    number = 0;

                    break;
                }

                number = number - variation;
                break;
            default:
                break;
        }

        return number;
    }

    private void setVehiclesUpdates(List<String> fileData, String number, int index, char operation) {
        try {
            OutputStream os = new FileOutputStream(COMPANY_STATUS_FILE);
            OutputStreamWriter osw = new OutputStreamWriter(os);
            BufferedWriter bw = new BufferedWriter(osw);

            for (int i = 0; i < 5; i++) {
                bw.write(fileData.get(i) + "\n");
            }

            if(operation != '-') {
                createVehicles(number, index);
            }

            bw.close();
        }
        catch (IOException e) {
            System.out.println("Ocorreu algum erro... :(");
        }
    }

    public void addOrRemoveVehicle(int index, int variation, char operation) {
        int lineInt;
        String lineToChange;
        String stringNumber = "" + variation;
        List<String> fileData = readCompanyStatusFile();

        lineToChange = fileData.get(index);

        lineInt = Integer.parseInt(lineToChange);

        lineInt = selectOperation(lineInt, variation, operation);

        String numUpdated = "" + lineInt;

        fileData.set(index, numUpdated);

        setVehiclesUpdates(fileData, stringNumber, index, operation);

        if(operation == '-') {
            clearFleet();
            setFleet(fileData);
            this.fleet.setAvailableVehicles();
        }
    }

    public List toTestFreight(double weight, double distance, double time) {
        return this.fleet.receiveFreight(weight, distance, time);
    }
}
