public abstract class NonFlex extends Vehicle {
    private Fuel fuel;
    private float efficiency;
    private float efficiencyFactor;

    public Fuel getFuel() {
        return fuel;
    }

    public float getEfficiency() {
        return efficiency;
    }

    public float getEfficiencyFactor() {
        return efficiencyFactor;
    }

    public void setFuel(String name) {
        fuel = new Fuel(name);
    }

    public void setEfficiency(float efficiency) {
        this.efficiency = efficiency;
    }

    public void setEfficiencyFactor(float efficiencyFactor) {
        this.efficiencyFactor = efficiencyFactor;
    }

    @Override
    public float getRealEfficiencyFactor(double weight, int fuelType) {
        double realEfficiency = ((double) efficiency) - (((double) efficiencyFactor) * weight);

        return (float) realEfficiency;
    }

    @Override
    public double calculateCost(double weight, double distance) {
        double realEfficiency = (double) getRealEfficiencyFactor(weight, 0);
        double totalFuel = distance / realEfficiency;

        return ((double) fuel.price) * totalFuel;
    }
}
