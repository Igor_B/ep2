public class Fuel {
    private static final float ALCOHOL_PRICE = 3.499f;
    private static final float GASOLINE_PRICE = 4.499f;
    private static final float DIESEL_PRICE = 3.869f;

    public String name;
    public float price;

    public Fuel(String name) {
        if (name.equals("alcool")) {
            this.name = name;
            this.price = ALCOHOL_PRICE;
        }
        if (name.equals("gasolina")) {
            this.name = name;
            this.price = GASOLINE_PRICE;
        }
        if (name.equals("diesel")) {
            this.name = name;
            this.price = DIESEL_PRICE;
        }
    }
}
