public abstract class Vehicle {
    public String name;
    private double maximumWeight;
    private float speed;

    public double getMaximumWeight() {
        return maximumWeight;
    }

    public void setMaximumWeight(double maximumWeight) {
        this.maximumWeight = maximumWeight;
    }

    public float getSpeed() {
        return speed;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    public double calculateTime(double distance) {
        return (distance / ((double) speed));
    }

    public boolean canCarryIt(double weight) {
        return weight <= maximumWeight;
    }

    public abstract float getRealEfficiencyFactor(double weight, int fuelType);

    public abstract double calculateCost(double weight, double distance);
}
