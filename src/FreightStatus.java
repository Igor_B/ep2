public class FreightStatus {
    public Object vehicle;
    public double totalCost;
    public double freightTime;

    FreightStatus(Object vehicle, double totalCost, double freightTime) {
        this.vehicle = vehicle;
        this.totalCost = totalCost;
        this.freightTime = freightTime;
    }
}