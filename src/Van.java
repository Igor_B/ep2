public class Van extends NonFlex {
    public static final String NAME = "Van";
    public static final int INT_TYPE = 2;
    private static final String FUEL_NAME = "diesel";
    private static final float EFFICIENCY = 10.0f;
    private static final float EFFICIENCY_FACTOR = 0.001f;
    private static final double MAXIMUM_WEIGHT = 3500.0;
    private static final float SPEED = 80.0f;

    Van() {
        name = "Van";
        setFuel(FUEL_NAME);
        setEfficiency(EFFICIENCY);
        setEfficiencyFactor(EFFICIENCY_FACTOR);
        setMaximumWeight(MAXIMUM_WEIGHT);
        setSpeed(SPEED);
    }
}
